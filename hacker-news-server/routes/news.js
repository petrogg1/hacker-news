const express = require('express')
const app = express()
const New = require('../models/new');
const axios = require('axios');


let updateNews = async () => {

    let newsOptions = {
        method: 'get',
        url: 'https://hn.algolia.com/api/v1/search_by_date?query=nodejs',
        json: true
    };
    try {
        let newsResponse = await axios(newsOptions);
        newsResponse.data.hits.forEach(currentNew => {
            if (currentNew.objectID) {
                New.updateOne({ objectID: currentNew.objectID }, { $set: currentNew }, { upsert: true }, (err) => {
                    if (err) throw err;
                    console.log("db update");
                });
            }
        });
        updateNewsTimer();

    } catch (error) {
        console.log(error);
    }
}

let updateNewsTimer = () => {
    setTimeout(() => {
        updateNews();
    }, 3600000)
}

updateNews();


app.get('/news', async (req, res) => {
    try {
        let news = await New.find().sort([['created_at_i', -1]]);
        return res.status(200).json({
            data: news
        });
    } catch (error) {
        console.log(error);
        return res.status(400).json({
            message: "Error"
        });
    }
})


module.exports = app;