const mongoose = require('mongoose');

let Schema = mongoose.Schema;

//{ type: Date, default: Date.now }

let newSchema = new Schema({
    /*
    name: {
        type: String,
        required: [true, the name is required]
    }
    */
    created_at: {
        type: String
    },
    title: {
        type: String
    },
    url: {
        type: String
    },
    author: {
        type: String
    },
    points: {
        type: Number
    },
    story_text: {
        type: String
    },
    comment_text: {
        type: String
    },
    num_comments: {
        type: Number
    },
    story_id: {
        type: Number
    },
    story_title: {
        type: String
    },
    story_url: {
        type: String
    },
    parent_id: {
        type: Number
    },
    created_at_i: {
        type: Number
    },
    objectID: {
        type: String
    }
});

module.exports = mongoose.model("New", newSchema);
