const express = require('express')
const app = express()
const mongoose = require('mongoose');


let startServer = async () => {
    await mongoose.connect('mongodb://database/hacker-news-dev', {
        useNewUrlParser: true,
        useUnifiedTopology: true,
        useFindAndModify: false,
        useCreateIndex: true
    }, (err, res) => {
        if (err) throw err;
        console.log("DB online");

    });

    app.use((req, res, next) => {
        res.header('Access-Control-Allow-Origin', '*');
        res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS');
        res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization");
        res.header('Access-Control-Allow-Credentials', true);
        next();
    });

    app.use(require('./routes/news'));
    const port = process.env.PORT || '3000';
    app.set('port', port);

    app.listen(port, () => {
        console.log(`Listen on Port ${port}`);
    })
}

startServer();


