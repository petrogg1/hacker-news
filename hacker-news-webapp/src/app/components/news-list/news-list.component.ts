import { Component, OnInit } from '@angular/core';
import { HackerNews } from 'src/app/Interfaces/hackerNews';
import { NewsService } from 'src/app/services/news/news.service';

@Component({
  selector: 'app-news-list',
  templateUrl: './news-list.component.html',
  styleUrls: ['./news-list.component.scss']
})
export class NewsListComponent implements OnInit {

  news: Array<HackerNews> = [];
  deletedList: Array<string> = [];
  monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
  newsError: boolean = false;

  constructor(private newsService: NewsService) { }

  ngOnInit(): void {
    //localStorage.clear();
    if (localStorage.getItem("deletedList")) {
      this.deletedList = JSON.parse(localStorage.getItem("deletedList")).data;
    }

    this.newsService.getNewsList().subscribe(res => {
      res.data.forEach(currentNew => {
        if ((currentNew.story_title || currentNew.title) && currentNew.objectID && this.deletedList.indexOf(currentNew.objectID) == -1) {
          currentNew.created_at = this.dateFormatter(currentNew);
          this.news.push(currentNew);
        }
      });
    }, error => {
      this.newsError = true;
    })
  }

  openNew(currentNew: HackerNews) {
    if (!currentNew || (!currentNew.url && !currentNew.story_url)) return;
    if (currentNew.url) {
      window.open(currentNew.url, "_blank");
    } else {
      window.open(currentNew.story_url, "_blank");
    }
  }

  deleteNew(currentNew: HackerNews) {
    if (this.deletedList.indexOf(currentNew.objectID) == -1) this.deletedList.push(currentNew.objectID);
    localStorage.setItem("deletedList", JSON.stringify({ data: this.deletedList }));
    let currentIndex = this.news.findIndex(indexNew => indexNew.objectID == currentNew.objectID);
    if (currentIndex != -1) this.news.splice(currentIndex, 1);
    console.log(localStorage.getItem("deletedList"));
    console.log(JSON.parse(localStorage.getItem("deletedList")));
  }

  dateFormatter(currentNew: HackerNews): string {
    if (currentNew && currentNew.created_at_i) {
      let currentDate = new Date();
      let newDate = new Date(currentNew.created_at_i * 1000);
      let dateText = newDate.getDate() > 9 ? `${newDate.getDate()}` : `0${newDate.getDate()}`;
      let hoursText = newDate.getHours() > 9 ? `${newDate.getHours()}` : `0${newDate.getHours()}`;
      let minutesText = newDate.getMinutes() > 9 ? `${newDate.getMinutes()}` : `0${newDate.getMinutes()}`;
      if (newDate.getFullYear() == currentDate.getFullYear() && newDate.getMonth() == currentDate.getMonth() && newDate.getDate() == currentDate.getDate()) {
        if (newDate.getHours() >= 12) {
          return `${hoursText}:${minutesText} pm`;
        } else {
          return `${hoursText}:${minutesText} am`;
        }
      } else if (newDate.getFullYear() == currentDate.getFullYear() && newDate.getMonth() == currentDate.getMonth() && newDate.getDate() == currentDate.getDate() - 1) {
        return `yesterday`;
      } else if (newDate.getFullYear() == currentDate.getFullYear()) {
        return `${this.monthNames[newDate.getMonth()]} ${dateText}`;
      } else {
        return newDate.getFullYear().toString();
      }
    }else{
      return "";
    }

  }

}
