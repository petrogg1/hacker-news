import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ApiResponse } from 'src/app/Interfaces/apiResponse';
import { HackerNews } from 'src/app/Interfaces/hackerNews';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class NewsService {

  constructor(private http: HttpClient) { }

  getNewsList() {
    return this.http.get<ApiResponse<Array<HackerNews>>>(environment.urls.news.list);
  }
}
